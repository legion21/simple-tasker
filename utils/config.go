package utils

import "gitlab.com/legion21/simple-tasker/task"

type Config struct {
	Tasks []task.ConfigTask `json:"tasks"`
}
