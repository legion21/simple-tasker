package task

import (
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"time"

	log "github.com/Sirupsen/logrus"
)

type Execute struct {
	Config   *ConfigTask
	runFlag  bool
	stopFlag bool
}

func (e *Execute) ExeCommand(wg *sync.WaitGroup) {
	parts := strings.Fields(e.Config.Command)
	head := parts[0]
	parts = parts[1:len(parts)]

	out, err := exec.Command(head, parts...).Output()
	if err != nil {
		log.Error(err)
	}
	if e.Config.LoggingResult {
		log.Info(string(out))
	}
	wg.Done()
}

func (e *Execute) Start() {
	if e.Config == nil {
		return
	}
	if !e.runFlag {

		// Set Actual Flags
		e.stopFlag = false
		e.runFlag = true

		// Run Threat
		go func(e *Execute) {
			if interval, err := strconv.ParseInt(e.Config.Condition, 10, 64); err == nil {

				timer := time.NewTicker(time.Duration(interval) * time.Millisecond)
				defer timer.Stop()

				keyСhan := make(chan os.Signal, 1)
				signal.Notify(keyСhan, os.Interrupt)

				processing := false

				for {
					select {
					case <-keyСhan:
						return
					case <-timer.C:
						{
							if e.stopFlag {
								return
							}
							if !processing {
								processing = true

								wg := new(sync.WaitGroup)
								wg.Add(1)
								go e.ExeCommand(wg)
								wg.Wait()

								if e.stopFlag {
									return
								}
								processing = false
							}
						}
					}
				}
			}
		}(e)
	}
}

func (e *Execute) Stop() {
	if e.Config == nil {
		return
	}
	e.stopFlag = true
}
