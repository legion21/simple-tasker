package task

type ConfigTask struct {
	Type          string `json:"type"`
	Condition     string `json:"condition"`
	Command       string `json:"command"`
	LoggingResult bool   `json:"logging_result"`
}

type Interface interface {
	Start()
	Stop()
}
