package task

import "strings"

// Create Task by Config
func New(cfg *ConfigTask) Interface {
	if cfg != nil {
		switch strings.TrimSpace(strings.ToLower(cfg.Type)) {
		case "execute":
			if len(strings.TrimSpace(cfg.Command)) > 0 {
				return &Execute{Config: cfg}
			}
		}
	}
	return nil
}
