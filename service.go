package main

import (
	"encoding/json"
	"io/ioutil"
	"path/filepath"

	"os"
	"os/signal"

	log "github.com/Sirupsen/logrus"
	"github.com/kardianos/osext"
	"gitlab.com/legion21/simple-tasker/task"
	"gitlab.com/legion21/simple-tasker/utils"
)

func main() {
	// Set Time Format
	timeFormatter := new(log.TextFormatter)
	timeFormatter.TimestampFormat = "2006-01-02 15:04:05.000"
	timeFormatter.FullTimestamp = true
	log.SetFormatter(timeFormatter)

	// Detect Execute File Path
	exeFile, _ := osext.Executable()
	currentDir, err := filepath.Abs(filepath.Dir(exeFile))
	if err != nil {
		log.Fatal(err)
		return
	}
	// Load Config File
	data, err := ioutil.ReadFile(currentDir + "/config.json")
	if err != nil {
		data, err = ioutil.ReadFile("/etc/simple-tasker/config.json")
		if err != nil {
			log.Fatal(err)
			return
		}
	}

	var cfg utils.Config
	err = json.Unmarshal(data, &cfg)
	if err != nil {
		log.Fatal(err)
		return
	}
	if len(cfg.Tasks) <= 0 {
		log.Fatal("Empty task list! Stop Simple Tasker!")
		return
	}

	keyСhan := make(chan os.Signal, 1)
	signal.Notify(keyСhan, os.Interrupt)

	log.Info("Welcome to Simple Tasker v.0.0.1")

	// Run Tasks
	startTasks := 0
	for _, taskConfig := range cfg.Tasks {
		if t := task.New(&taskConfig); t != nil {
			t.Start()
			startTasks++
		}
	}
	if startTasks < 1 {
		log.Fatal("Empty start tasks! Stop Simple Tasker!")
		return
	}
	for {
		select {
		case <-keyСhan:
			{
				log.Println("<SIGINT> Stop Simple Tasker")
				return
			}
		}
	}
}
