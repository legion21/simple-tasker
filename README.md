## Depending on the package

- Install Git and Mercurial*

        * - optional

```bash
go get -u github.com/kardianos/osext
go get -u github.com/Sirupsen/logrus
```

## Setup GitLab SSH

```bash
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
```

## Build Cross

```bash
mkdir build
./build.sh
```

## Build and Run

```bash
go build -o service.exe gitlab.com/legion21/simple-tasker
./service.exe
```



