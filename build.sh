#!/bin/sh

cp config.json build/config.json
cd build
GOOS=darwin GOARCH=386 go build -v -o darwin_service gitlab.com/legion21/simple-tasker
GOOS=linux GOARCH=arm go build -v -o linux_service_arm gitlab.com/legion21/simple-tasker
GOOS=linux GOARCH=386 go build -v -o linux_service gitlab.com/legion21/simple-tasker
GOOS=linux GOARCH=amd64 go build -v -o linux_service_64 gitlab.com/legion21/simple-tasker
GOOS=windows GOARCH=386 go build -v -o win_service.exe gitlab.com/legion21/simple-tasker
GOOS=windows GOARCH=amd64 go build -v -o win_service_64.exe gitlab.com/legion21/simple-tasker
